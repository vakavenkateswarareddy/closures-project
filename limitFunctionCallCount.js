function limitFunctionCallCount(cb,n){
    let count=0
    function invokeCb(...args){
        count=count+1;
        if(count<=n){
            return cb(...args);
        }else{
            return null;
        }

    }
    return invokeCb;
}


module.exports = limitFunctionCallCount;